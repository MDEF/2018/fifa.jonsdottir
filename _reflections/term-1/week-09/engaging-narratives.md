---
title: Engaging Narratives
period: 26 November - 2 December 2018
date: 2018-12-02 12:00:00
term: 1
published: true
---

*With Helen Corcoran & Bjarke Calvin*



We all loves stories. Humans are social beings and as such we are hardwired to take notice when encountering an engaging story. Storytelling is one of the most powerful tools that a designer can wield, but becoming skilled at telling stories takes a lot of practice. When well constructed, stories can be used to sway audiences and even whole societies in one way or the other. So the phrase “with great power comes great responsibility” applies.

This week with Helen from Kickstarter and Bjarke did not feel too intense or serious but important none the less. They went over concepts and background to keep in mind when constructing a compelling narrative. Being a communication designer it all felt familiar but it is always good to revisit strategy and also to see storytelling from a different perspective.  

Where do you start when telling the story of your project? It can seem like the easiest part of the whole process because you know the ins and outs of every detail but when you need to get the core of your work across to an unfamiliar audience while keeping their attention is harder than it looks. The impact of many great projects has probably been lost in the deserts of PowerPoint templates.
<br>
<br>
![]({{site.baseurl}}/what.png)
<br>
### Shameful self promotion?


Just uttering the phrase self promotion is enough to make most creators cringe. When you create something it is more than just posting a photo of yourself, it’s sharing a part of your soul with the world and that can be really scary. When you pass that fear and have started to share your work you might need to start managing your work and your time like a business, as unappealing as that sounds. Personally I feel very uncomfortable putting my work on a platform where others can see and interact with, not because I feel like my work is not good enough but because it is a level of intimacy that I am not willing to share on a public platform. I am on the other hand much more comfortable and happy to promote other people and their work. It can be very personal but when you are not the one pouring your soul out to the world it feels easier to do. When I explore my feelings towards sharing my work online it stirred some mixed emotions. But as we write down our thoughts and reflections every week it feels a bit more natural to share.


[The{{site.baseurl}}ive Independent](https://thecreativeindependent.com/)
<br>
<br>
![]({{site.baseurl}}/comes.png)
### Stories in Science


The environment for scientific study has been more interested in publishing and educating within their own fields so the focus has been on other things than making that knowledge approachable to non-specialist audiences. That can make new and emerging research unavailable and make progress much slower than it could be. So if scientists would pair up with designers and storytellers I think public understanding of the natural world would increase with beneficial consequences, for people and the planet.


 It can be quite tricky to relay that kind of technical knowledge in an engaging way because there needs to be a high level of understanding from the scientist and also the designer. As the flow of information in our daily lives becomes faster and attention spans become shorter, the need for attention-grabbing headlines is on the rise, so those who communicate science pick up on the most strange and often negative stories, leaving the positive on the sidelines. Prompting the question: How can we make people interested in the positive (maybe a little less interesting) side of science?



One way of making abstract and complicated concepts in science understandable is by creating metaphors for them, like the Nucleus in a cell as a library from Biology Zero. Metaphors can be tricky and have to be constructed carefully so they don’t confuse and do more harm than good. So it should be a careful construction of words, keeping a delicate balance between interest and accuracy of information.



*The language we use to make a better world matters; words matter; metaphors matter. Words have consequences, even ethical, social and legal, as well as political and economic consequences. They need to be used with care and be studied with care. They need to be used ‘responsibly’*

\-       Brigitte Nerlich
<br>
<br>
![]({{site.baseurl}}/next.png)

### Reflections

Having worked with storytelling as my medium before I know how important it is to create an engaging narrative around your project to grab peoples attention. But sometimes it feels false somehow. It feels wrong to play with peoples emotions because you are creating a narrative around things that have not been tried or tested. Even though it might feel manipulative, it is crucial that we that strive for a better and more equal future use every strategy that is out there to reach those goals. Just keep in mind that you always have to be critical of the words you use and the information you share.  


[On language used in science](http://blogs.biomedcentral.com/on-society/2018/11/29/why-we-should-care-about-the-language-we-use-in-science/)

[Metaphors in science communication](https://www.biomedcentral.com/collections/sbmi)
