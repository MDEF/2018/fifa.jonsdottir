---
title: Biology Zero
period: 8-14 October 2018
date: 2018-10-14 12:00:00
term: 1
published: true
---

# Biology Zero
*With Dr. Nuria Conde and Jonathan Minchin*

I was really looking forward to this week and it did not disappoint. I have a background in biology so everything we did this week I had done previously to some extent. Experiencing a whole week of biology basics for beginners was really interesting, I especially liked the use of metaphors that connect complicated subjects to more relatable objects or systems from our daily lives like the transcription of DNA inside the nucleus with copying a page from a book in a library.<br>
We also got a glimpse of what synthetic biology can be used for in an art or design context. The possible applications of biology in the future is something I am very interested in and am excited to be a part of.

![]({{site.baseurl}}/BioNotes.jpg)
*A collage of my notes during this week*

## **Hypothesis**

People tend to view space as something inanimate or dead. Their environment is something that is maintained for them but what if we could engage people in maintaining their surroundings and making them aware of the optimal conditions of a space by introducing “guide organisms”? Introducing a living bio-sensor into an environment could help people understand that they can impact their space by interacting with the sensor/organism.
Some organisms like fungi and slime molds are very sensitive to their environments and react to stimuli like light and temperature in different ways. Those reactions can be calculated and translated so it’s easily understood by humans.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Companion organisms will be enablers and enhancers of human senses and allow for greater awareness and control of people’s immediate environment such as a school classroom.**


## **Method**

1. Optimal conditions for humans identified.
2. Organism chosen that survives/thrives within the human parameters.
3. Suitability of the organism concerning growth, reproduction and storage defined.
4. Human – organism relations defined and established
5. Best cultivation media and homeostasis for organism created.
6. Stimuli tested on organism to gauge reaction.
7. Organism reaction to the established stimuli tested and measured.
8. Reaction of organism translated to intuitive outputs.
9. Success of human – organism interaction analysed.
10. Human awareness of the environment measured.

## **Reflection**

This week we discussed what role biology will have in design and our future and it is clear to me that biology will have an increasingly important role in the way we construct and manufacture our material world. Understanding the living systems of our immediate environment is vital and can be a great source of inspiration for invention and creation. As we move from a passive to an active role in evolution with new technologies like Crispr Cas9 the possibilities for improvement are endless.
Even though we could create just about anything with those methods there are social, economical and political limitations that need to be considered and scientists must contend with governance and the elusive public opinion to make change happen. By introducing and incorporating biology to other disciplines we might make the integration of new ideas and technology to a mass audience. So this was a question I asked myself:<br>
<br>
What role should the government have in the implementation and creations of synthetic biology?
<br>
<br>
![]({{site.baseurl}}/bioicons.jpg)
<br>
<br>
  *Choice*<br>
A question was asked in the class: Could you give a human wings? In theory, yes, with gene editing and bio printing almost anything is possible. But the question left unasked was: should we? That question opened up a whole line of questioning for me.

  Can we and should we change things?
  Who decides what will be changed?
  What are the possible benefits?
  Are there negative consequences that need to be considered?
  What are the limitations for that change to happen?
  How will society embrace the changes?
<br>
<br>

  *Inspiration*<br>
The more scientists research the systems and mechanics of the living world they continue to discover layers upon layers of complexity. The systems that are a part of our reality and we often take for granted have evolved through trial and error for millions of years, so when designing with biology it is important not to reinvent the wheel but to look at some examples with similar functions and take inspiration from there. The subject of bio mimicry is very interesting and studies from that field have advanced transportation systems and robotics, so maybe nature can inspire us to design more functional and sustainable cities in the future.
<br>
<br>

  *Safety*<br>
Having done an internship with a DIY biology lab and working with Dr. Nuria Conde this week has made me more passionate about science and the fact that science should be more accessible, but society has quite a negative view of synthetic biology because of how governments and media portray it. The people and communities actually bringing science to the people hold safety above everything else. Safety of the participants and the subjects they are working with. So DIY biology is safe is a message that I think is very important to get out there.
<br>
<br>
## **Future**
In the future I want to integrate living things into our everyday life whether it is through platforms or design. it will be an important step to make bigger changes possible.
<br>
<br>
### References

[Biohackers are about open-access to science, not DIY pandemics. Stop misrepresenting us](https://www.statnews.com/2018/06/04/biohacker-open-access-science/)<br>

Icons<br>
Man by Gerard Higgins from the Noun Project<br>
Wings by Oksana Latysheva from the Noun Project<br>
Sprout by GreenHill from the Noun Project
