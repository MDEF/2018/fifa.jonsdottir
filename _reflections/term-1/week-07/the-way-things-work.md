---
title: The Way Things Work
period: 12-18 November 2018
date: 2018-11-18 12:00:00
term: 1
published: true
---

# The Way Things Work

*with Guillem Camprodon, Victor Barberan & Oscar Gonzales*
<br>
<br>

**How much do you actually think about <br>
the technology that you use every day?**

**Do you know how things work?**

***Do you want to know?***
<br>
<br>

This week was an introduction to open source hardware and physical computing - starting the course we had a lot of fun dismantling some every day gadgets like printers and coffee makers to see and try to understand the integral parts of machines we take for granted. We interact with so many different machines in our daily lives without really understanding what they are made of and how they actually work. Even though it would be great if everyone understood the mechanics of every appliance and gadget they own, it feels like an unobtainable task and as the world becomes more technology oriented and complex it becomes even more daunting to try to understand what is going on.

While taking apart the printer we discovered that the moving parts within the machine are not many and it is mostly based on small motors. It was quite hard to take the printer apart because it is not designed to be opened up. When we were dismantling it some of the joints broke, so it would have been impossible to put back together if that were the intention. One of the most interesting insights from taking apart the printer was looking at the cartridges and discovering that they are programmed to only match the corresponding printer and when it has finished a certain number of prints / jobs it won't print anything else, whether there is leftover ink or not. It also makes it impossible to refill the cartridge unless you can hack the chip or bypass it in some way. That is the business model of printing companies like HP, Epson or Canon. They sell printers very cheap and then they make their money by selling ink.

Most of the parts inside the printer were pretty basic and could potentially be re-used for other gadgets or projects.  We tried using the motors in our final projects but we were not able to make them work, so after dismantling all the electronics we had a lot of unusable electronic parts and plastics. It feels like these machines are designed to be completely disposable. Just looking inside of them and not being able to fix them or put them back together feels very wasteful. Now the sad, empty plastic husks of the printers are sitting in a corner of the classroom waiting to be discarded.<br>
<br>

[![TWTW]({{site.baseurl}}/TWTWicon.png)](https://vimeo.com/302714698)
*A small compilation video of looking inside the printer - Music by Gabor*



## Physical Computing

I will admit that I am not the most technology adept person so when they were talking about systems and networks a lot of that information didn't sink in the first time, but I tried to follow along as best I could. Coding languages and Networks are built on logic and that is something i have always struggled with, but
there was one thing that really helped me. Victor showed us that when coding a function, if you set up a flow chart explaining what you want it becomes much easier to write the code.
The visual structure and language of flow charts makes it easier to comprehend the logic.<br>
<br>
![]({{site.baseurl}}/FlowChartExplanation.jpg)

## Interesting Interfaces

The first idea for our physical project was to use a humidity sensor to measure the state of our class plants and connect it to a direct output like sound so basically making the plant ask or sing for water when the humidity of the soil got below a certain value.

 Giving the plant a more tangible indicator of its hydration was something I really feel is interesting because we know plants are living things and they do indicate when they are dehydrated, but it can take days for them to tell you that they need water. So when we were able to connect the humidity sensor to the soil to a LED light it made the connection between us and the plant more tangible and more personal in a way. I would like to explore further how we can develop these kinds of indicators and connections with other living things.

 **Humidity sensor - soil - LED light** -->

**Arduino - plant - wifi - fart noise machine**

 Our task for the week was to work either on an input or an output that would later be connected to another project in the class so we decided to try making the plant a capacitive sensor (reactive to touch). It took a lot of wiring and rewiring, connecting the plant and getting the WIFI chip to work but my group managed it in the end. When we actually connected the touch to a direct output, in this instance Sairas fart machine we were all stunned! It was so much fun to be able to cause another machine to work even though the machines were not physically connected. Unfortunately no one got the first reactions on video but they were shock, and laughter.

![]({{site.baseurl}}/capacitorplant.jpg)
<br>
<br>
![]({{site.baseurl}}/leafstickers.jpg)
<br>
As I was not the most coding oriented in our group I decided to make some visuals for our project. I made these stickers with the  name and the attitude of our team. The stickers were very successful and it got the rest of the class even more excited about our project. The stickers also showed how a small effort on the visuals can make a project seem more professional and that can generate more interest. Now the stickers are spread all around, on notebooks, phones and laptops and is a reminder of the good time that we had this week. The square image has also become the background for a website Ilja made (see below) that is dedicated to our project, where you can observe real time readings from the plant when it's connected.

[Wet my leaf](https://wetmyleaf.github.io/?fbclid=IwAR1DTe0xUmOGPXnMV6EI-Iaq033HtbirakY1CqwpPIw2cPQxGSIRksODIA8)

## Reflections

The moment we touched the plant and it made the fart noise machine go off was one of the most eye opening moments of the last few weeks for me. We had been working towards that goal for the whole week and we knew that something was going to happen when we touched the plant, but when it actually happened it was amazing. Feeling that immediate reaction from touching the plant was something that we could not have speculated. <br>

For me it sparked a sense of wonder both from being able to connect the plant to another machine over a private wifi network but to see the excitement of everyone in the class when they tried the connection. I want to explore if an immediate reaction like that could be applied when connecting people with biology or climate change because when you are directly impacted by your actions it feels more powerful than when you experience the changes over a long period of time.<br>

I really enjoyed the way this workshop was set up. Starting by breaking things apart and then working in small groups on projects putting things together and connecting with the others over the shared network to actually make electronic magic happen. I am very glad that we worked in groups because I don't think I could have written the code and connected everything on my own. But this workshop demystified a lot of the inner workings of open source hardware so maybe it won't feel so foreign next time.
<br>
<br>
#### Other cooky projects:
[Makey Makey](https://vimeo.com/279920496) is a project developed by a team at MIT to introduce physical computing to people. With it you can make any conductive material into some interactive fun.<br>

The [Furby Organ](https://youtu.be/GYLBjScgb7o) project is one of my all time favorites! It has a beautiful sound, jaw dropping wiring and might cause you nightmares all at the same time.
