---
title: Design for the Real Digital World
period: 15-21 October 2018
date: 2018-10-21 12:00:00
term: 1
published: true
---

# Design for the real digital world
*With Ingi Freyr and Francesco Zonca*
<br>
## **Process**
In design for the real digital world we had the objective of reimagining and redesigning our classroom with furniture and leftover materials we found on the streets of the city, using the tools and techniques of digital fabrication. We split into teams, making sure that each team had people with different backgrounds and experiences. The teams constructed proposals on how the classroom should be organised and presented it to the rest of the class. The basic areas we needed to keep in mind were:<br>
<br>
Coffee corner/relaxation area<br>
Individual work spaces<br>
Presentation area<br>
<br>
Everything else was up to us to reimagine.<br>
<br>
Having total freedom to redesign the classroom we thought about how a class is normally set up with the teacher standing in front of the class giving a lecture. We wanted to rearrange the space to make learning from each other a priority. To make the space more comfortable for each individual we sketched out some ideas about seating arrangements.<br>

![]({{site.baseurl}}/classlab.jpg)

We took inspiration from the materials that we had gathered and sketched out possible scenarios from what we had. From that we made a proposal about storage space, a separating wall and alternative seating. Me, Jess, Emily and Adriana worked on the storage and Katherine and Gabor worked on the wall.<br>
<br>
![]({{site.baseurl}}/storage collage.jpg)
<br>
![]({{site.baseurl}}/arrangement.jpg)
<br>
![]({{site.baseurl}}/construction.jpg)
<br>
![]({{site.baseurl}}/storagefin.jpg)
<br>

We CNC'd holes in the drawers to fit the beams and a custom piece to fix the table on the bottom, 3D printed the missing leg to the table.
Thinking of the product life cycle it is very easy to rearrange and disassemble if it is needed. <br>


## **Reflection**
<br>
This project really opened up my eyes to the possibilities of repurposing and taking things that were of no use to someone else and with some care they can serve a new purpose and be of value to someone else. It was also very satisfying to experience our sketches and ideas become reality in a matter of days.
to transform the classroom from a normal chair and table setup to an attractive, functional and personalised space.<br>
<br>
Having never used any of the digital fabrication tools I felt like my imagination was a bit limited. It was an important introduction to these tools and it was a lot of fun to explore these new ways of fabricating. It will most likely take some time and practice to start incorporating these techniques into my design process but I’m excited about the possibilities.<br>
<br>
This week was a lot of fun and it felt so satisfying being able to take scraps and turning them into usable furniture but I had a nagging question in the back of my mind the whole time. Being a design student, I have never done anything like this before so how plausible is it to shift the mindset of society from buying “single use” furniture to thinking about the life cycle of the product?<br>
<br>
Can we design a system or find a way to make it more attractive and accessible for anyone to repurpose their old/broken down furniture?
