---
title: Computer controlled machining
period: 6 - 13 March 2019
date: 2019-03-07 12:00:00
term: 2
published: true
---

## CNC - Make something big 🦖

- Assignment: make something big in groups of two. We got one sheet of 1220 mm x 2440 mm x ~15 mm sheet of plywood and the
- challenge is to make something three dimensional without using any external binder like glue or screws.
- Try to use as much of the material as possible.

The CNC is a subtractive process and is usually used for producing big durable and strong parts. Using one homogenous material is beneficial because it retains the molecular structure throughout the whole piece. It can be stronger than fusing parts together.

Subtractive processes can create a lot of waste so you should always try to use your material as efficiently as possible so nesting is super important. When working with big and thick materials one minute of extra cutting will make a huge difference when you are producing more parts and waste material adds up really fast.

Try your project first on the laser cutter in 3 mm cardboard in 1:5 so you can see how your model will look without wasting material and time on the CNC machine. This should be your process for prototyping, start small and fast and when you are happy you can take your design to large scale.

technical specs:

![]({{site.baseurl}}/end mill.png)

The end mills are made out of tungsten carbide or high speed steel (cobalt steel alloy) they are durable but you still have to make sure that you don't drop them on the floor because they might break. It is also very important that you choose a correct end mill for the material you want to work with. The shape and number of flutes (blades) are specifically designed for different jobs.

Feed rate is very important. it is the relation between the speed of the cut, material and flute number. The feed rate tells you how many chips are formed and controls the cooling of the machine. If it is wrong it you can damage the machine, the mill or set fire to the whole thing.

Straight flute - Good for softer materials - plastic, MDF, plywood.
Up cut - better for fast cutting - Metals, MDF
Down cut - compressing material - Soft wood
Compression - presses up and down (pressing to the center) - for various materials

Single flute - allows for greater chip load in softer materials
Double flute - smoother finish in harder materials
Triple flute - even better finish in harder materials

As the number of flutes increases your feed rate should increase as well because more flutes equals less chips for cooling.

[Further knowledge](https://wiki.imal.org/howto/cnc-milling-introduction-cutting-tools)

## Prepping the cut

Make sure you have measured your material very well, especially the thickness. The more measurements you have the more accurate your cut will be. The plywood boards can vary from 14,8 mm - 15,2 mm so if you measure your board you can find a middle ground and adjust the parameters accordingly.

Securing the material
You can use clamps, screws or tape, depending on your material just make sure that you know where you placed them so you don't ruin the tools by running them over rogue screws.

It is vital that you don't rush when preparing the material for the cut. If you are unsure, put more screws. You can prep the screw placement in the program by making small indents in the material with the end mill.

Climb cutting - cutting and sanding at the same time
Conventional cutting - for less powerful motors (good for foams and wax)
if you are not sure which method to use you can test it on your material and it will tell you

If you are cutting all the way through you have to cut through the material + 0,2-0,5 mm so you cut into the board below.


## Safety 🔥 🔥 🔥

Make sure you double check every single step of the setup religiously. Every single part of the machine can cause a fire or cut off extremities.😳

- Tie hair back 👧
- No loose articles of clothing 👕
- Closed shoes 👟
- Be alert and aware always 👀
- Safety goggles 🙈
- Ear protection 🙉
- Never cut alone 👯
- Know where the emergency stop buttons are! 🆘

Knowing how the sounds of the machine is also really important. If the machine is generating too many chips it will sound low and sluggish but if it's not generating enough chips to cool the machine it will give off a high pitched scream like a hungry baby.

## Laser cutting a test

The “Something BIG” - when you think of something big what is more perfect than a dinosaur?

Me and Sylvia decided to work together and make a pet dinosaur! We found some schematics online and we decided to go with the T-Rex

We found a template on Instructables - I took the PDF and imported it into Illustrator. The template was too big to fit onto our one sheet of plywood so we had to resize it. I imported all the shapes to illustrator and made sure that all the shapes were joined by adjusting the lines. I then nested and resized the shapes (making sure I was resizing everything equally) until all the shapes fit onto the sheet.

![]({{site.baseurl}}/dinonest.png)

- [Wooden T-Rex model - original files](https://www.instructables.com/id/Build-a-6-0-tall-Wooden-T-Rex-Model/)
- [Detailed instructions](https://www.instructables.com/id/Cardboard-Dinosaur-Puzzle/)

Sylvia took the illustrator file and imported it into fusion where she made all the tabs parametric (15 mm) There was a problem with the file because even though all the shapes were joined in illustrator the lines were all separate in fusion which was a problem. After making the tabs parametric Sylvia exported the file from fusion to Rhino and had to join all the lines by hand. It was really strange that there were so many tiny lines that needed to be trimmed. It was really tedious work to join everything so we took turns until all the shapes were whole. The layout was ready for a test and we wanted to laser cut the puzzle to see if it would all fit together so we needed to scale it down to 1/5 for the laser. The scaling is done in rhino with these steps:

- Copy
- Scale 2D
- Origin point
- 0,2 for 1/5th - Laser cut
- Export selection DXF

We found a piece of 3 mm plywood that we could cut from. We used wood instead of cardboard because we wanted to keep the models for ourselves. I cut the material to fit, we secured it with tape and then we prepared the file in rhino to cut. We set the power to 65, speed to 1,5 and frequency to 1000. We started the cut and everything looked good but when it was finished we took it out and saw that it had not cut all the way through. we did not do a test in the beginning which was a mistake because we wasted time and material. We reset with a new sheet of material and set up the test. it was really difficult because the circle we wanted to cut did not show up when we sent it to print so it took a long time and we had to get help to be able to do the test. When we finally were able to do the test and we needed to lower the speed down to 1 for it to cut through.
So we started the cut again and it worked with power 65 and speed 1.0.

![]({{site.baseurl}}/laserdino.png)

The model looks amazing in the small scale but now i’m a bit concerned about how big it’s going to be but they said make something big so we’re gonna…

## CNC

After laser cutting the model we realised that there was another problem with the setup. I had nested the parts too close together for the CNC. It worked for the laser but the CNC needs more space in between the parts to get a correct result. The space between the parts should be no less than 22 mm if you are using the 6 mm end mill. Because of that we needed to resize the parts again and make them smaller so they have enough space in between them. But that also meant that we needed to do the fusion rhino thing all over again.

Resize in rhino and change the parameters of the tabs in fusion again then export it back to rhino to join all the lines again. It is a real hassle and Sylvia was so lovely to do that whole thing.

## Rhino cam

When all the tabs had been adjusted, pieces moved to have adequate space between and a border to have space for screws it was ready to import to rhino cam. In Rhino Cam we prepare the file for the CNC cut.

We were made aware of that we had not added "dog-bones" to the design. We added 6mm diameter circles on each side, closest to the bottom of the tab. We have to do that because the end mill is circular so it needs space to cut the bottom edge of the tab. We added the dog-bones in the rhino file. Moving and trimming every single one. After the trimming we joined the shapes. Joining the curves is a mess - use Make 2D and Closed curve!

![]({{site.baseurl}}/rhinocam.jpg)
_creating selections for profiling, pocketing and engraving._


When all the curves were closed we could import it into Rhino cam to prepare the file for cutting. We need to adjust the settings for 3 different jobs

- Engraving: creating the indents for screws. When you engrave for screws you know exactly where the screws are and don't have to worry about the machine milling into them.

- Pocketing: Mills any holes you need in your design. Make sure that you are milling on the inside of the curve so it doesn't mill too much material away.

- Profiling: Mills the outlines. Make sure that you set the cut to go 0,2 - 0,5 mm below the thickness of your material so it definitely cuts through.

When you are ready to mill you need to place the plywood sheet on the machine and set the x and y coordinates. when that's done you have to set the Z axis. You set the Z axis by placing a button on the material and running a script from the computer. You have to be very careful to position the button correctly so the end mill hits it dead center. If it is a bit off it can break the end mill.

![]({{site.baseurl}}/cncbutton.jpg)

When you have set the axis you are ready to start milling. Start with the engraving. When the machine is done engraving move the machine arm away from the sheet to the far corner. Take screws and start fastening the sheet to the board underneath. When drilling the material lifts up a bit so it is good to rewind the screw just a bit and then drill down again so it sits flush to the underlayer.

When everything is secure you can start milling. It will start pocketing first and then do the profiling.

- Remember to turn on the vacuum so you have less to clean up afterwards.

![]({{site.baseurl}}/cncprogress.jpg)

When the cutting is done you can remove all the parts from the sheet. we broke the tabs off and sanded the parts, both by hand and with a rotary sander.

We did not put an offset on the tabs in the design so we had to sand some joints and hammer the parts to make them fit but it worked. We needed to sand a couple of joints so the dinosaur would stand correctly but it's ready!

![]({{site.baseurl}}/dinoscale.jpg)

[Dino 3DM file]({{site.baseurl}}/TREXcnc.3dm.zip)

☠ No humans were harmed in the process of this project. ☠
