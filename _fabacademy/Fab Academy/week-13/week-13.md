---
title: Networking and communications
period: 24. April - 1. May 2019
date: 2019-05-01 12:00:00
term: 2
published: true
---
![]({{site.baseurl}}/facegrid.png)

## Synchronous / Asynchronous communication (serial)

dependent on what you need you choose different ways of communicating, according to distance, uses and needs.

- asynchronous - data is transferred without support from an external clock signal. Minimises needs of wires but is less reliable.

- synchronous - Pairs data with a clock signal so all devices on a synchronous bus share a common clock. Faster transfer - requires at least an extra wire.

## Serial / parallel

- parallel interfaces - transfer multiple bits at the same time. Waves of data. high speed but a lot of wires.

- serial interfaces - stream data with a reference signal. One bit at a time. one to four wires. USB uses 4 wires.

## RX-TX

rx-tx are the most common way of serial communication - it requires only two wires apart from a common ground.

data is sent asynchronously

## NRF24 Communication
NRF24 is a Transceiver and not a receiver transmitter. It has a package receiving confirmation. It will send the data over and over again until the other device receives the data package. It is not using serial communication but it sends data very precise.

## HC12 Communication
HC12 is a low frequency device which sends with half duplex wireless serial communication.


## Creating a network in class

I worked in a team with Jess, Veronica and Saira.

We connected the 12C transceiver to the arduino board according to the [Hackmd example](https://hackmd.io/s/B15x5dn9V)

![]({{site.baseurl}}/wiring transceiver.png)

![]({{site.baseurl}}/transceiver-arduino.jpg)

We downloaded the RF24 library and imported it
Sketch - Import library - add library

![]({{site.baseurl}}/manage libraries.png)

![]({{site.baseurl}}/libraries.png)

Check that you are using the correct board. Set up Arduino as Arduino Genuine Uno and Arduino as ISP. Arduino as port.

![]({{site.baseurl}}/board management.png)

## Network

![]({{site.baseurl}}/network layout.jpg)
this was the layout from the class (drawn by Oscar Gonzales)

We were one of the sending teams so we have to have the correct address to send to the faculty receiver. And as the senders we used the sender code from the [Hackmd](https://hackmd.io/s/B15x5dn9V) example. The code worked on the first try and we were able to send a signal to the tutors.

![]({{site.baseurl}}/sender code.png)

When we tried to use the Receiver code (from the same [Hackmd](https://hackmd.io/s/B15x5dn9V) example) it did not work. We kept getting errors on the example and could not compile it onto the arduino. We tried importing other libraries and changing the code slightly to try to get it to work. We finally changed computers and the code worked without a problem. 

![]({{site.baseurl}}/receiver code.png)

We changed the addresses of each group ranging from 0x000001 - 0x000004. Ours was 0x000003. When we opened the serial monitor we did not see any messages, but we figured out that we had to match up the number on the serial monitor and the baud number. We had to match it to 9600 that we were using in the code and on the bottom of the serial monitor. When that was done we were able to receive the signal.

![]({{site.baseurl}}/Networking10.png)
