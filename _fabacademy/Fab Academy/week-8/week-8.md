---
title: Embedded programming
period: 13 - 20 March 2019
date: 2019-03-20 12:00:00
term: 2
published: true
---

This weeks assignment was to program the hello echo board that I had milled and soldered in the [electronics design assignment](https://mdef.gitlab.io/fifa.jonsdottir/fabacademy/week-6/). The board has two LEDs, one that indicates that the board is working and the other can be programmed to blink. The Hello Ecco board includes an ATtiny44 microcontroller that makes it possible to program the board.

To be able to program the board you need to read the ATtiny44 data sheet. The data sheet lists down the purpose and position of each pin and how data is stored within the micro controller, available [HERE](http://fab.cba.mit.edu/classes/863.09/people/ryan/week5/ATtiny44%20Data%20Sheet.pdf). The ATtiny44 is basically a small brain that is able to remember and execute programs that it stores.

![]({{site.baseurl}}/datasheet.png)

To program the board you have to use C which is a coding language that allows you to program the board with instructions like making LEDs blink.

## Starting the programming process

I started by checking all the connections of the Hello Ecco board and also the connections of my FabISP that I was going to use to program my board. Using the multimeter I checked the connections of both boards and they seemed to be fine.

I proceeded by starting the preparation for programming. To connect the board to start the programming I needed to open the Arduino program on my computer and install the ATtiny library. You install the library by going to Tools - Manage libraries - then search for ATtiny and press install.

![]({{site.baseurl}}/attinylibrary.png)

I connected my FabISP via the micro USB port and then connected it with a 6pin cable. I connected the Hello Ecco board to power (the first LED turned red as an indication of it being on) and then started adjusted the settings to be able to program it.

I opened Arduino and in Tools you select

- Board: ATtiny 24/44/84
- Processor: ATtiny 44
- Clock: External 20 MHz - the hello ecco has a 20 MHz clock on the board
- Serial port: My serial port that showed up on the list.
- Programmer: USBtinyISP

When everything is selected I pressed Burn Bootloader. What that does is program the micro controller through the USB serial port.

When I tried burning the bootloader I received an error message that I didn’t understand.

![]({{site.baseurl}}/errormessage.png)

I asked Xavi and he was able to figure out that it was my FabISP that was not working. It was not communicating with the serial port. I could not figure out why the ISP wasn’t working so I changed it out for the AVRISP.

![]({{site.baseurl}}/eccoconnect.jpg)

Changing the ISP to the AVRISP was quite easy I just needed to change the settings in Tools. I changed the programmer from USBtinyISP to AVRISP mkII. After doing that I tried burning the bootloader and it worked without a problem.

## Programming

The goal of programming the board was to get the second LED to blink with code that was sent to the hello ecco board.

Starting with the basic Blink code provided as an example by Arduino but adjusting the parameters of the pins from the ATtiny pins to the Arduino ones. That can be done by using [THIS conversion sheet](https://fabacademy.org/2018/docs/FabAcademy-Tutorials/week8_embedded_programming/attiny_arduino.html). The LED on the Hello Ecco board is connected by the VCC pin which is number 6 - that means pin nr 7 for Arduino.

Having adjusted the code the LED started blinking!

Picture of blinking LED

Here is the code:
![]({{site.baseurl}}/blinkcode.png)

Link to blinking code...
