#Design Dialogues

This term has been one big rollercoaster but I have gained valuable insights into my area of interest. It started as a vague notion of connecting people with their environments. It then lead to interesting and exciting explorations of our relationships with indoor plants and ultimately our own bacteria. The presentation was a collection of all the ideas and experiments connected to my thoughts and feelings surrounding my own bacteria.

One of the weird things I started thinking about connected to microbiomes is how different we treat our internal biomes with more care than our external ones. We know that eating probiotics is good for us we use antibacterial soap and other products that rid us of all the bacteria on our skin, bad and good. What if I could make people take care of their external biomes as they do their internal.

One of the trends right now is drinking Kombucha which includes active bacteria and yeast cultures. People know it's good for their stomachs and their digestion so how could I transfer that to an external context? I am not completely sure people know why it's good to drink Kombucha but maybe they don't need to know exactly why to start taking care of themselves in a more active way.

My presentation was split in two, one part was the speculative artefact called the biome scanner, the other part was being covered in gold glittery makeup to show my experiments about living with my own bacteria.
<br>
<br>
### Biome scanner:

The biome scanner where you scan your hands and get your personal biome profile. It lets you know what problems you could experience if there was an imbalance in the good and bad bacteria.

-       Where would people get this information?

-       Who can access that information?

-       Would it be a part of your medical file?

-       Would it be connected to skin care or cosmetics?

-       Would it be a part of online dating profiles?

-       If that information was a key factor in choice of mate would that be a good thing or a bad thing?

-       Do people want to know specifics about their bacteria or would it be enough to just boil it down to type and enable people to seek advice with their own personal code?

-       Could creating a visual language that represents the different strains of bacteria make it more approachable for people to interact with?

-       Who should have a say in how that visual language is constructed?
<br>
<br>
#### Limitations:

-       There are different groups of bacteria that live on our bodies, only in specific areas so just scanning your hand would never give you an accurate account of what bacteria you have on your body.

-       There is also is also difference in information exchange within the microbiome, one is transmittance -  the things we experience and then signaling which is communication within the biome. I have been focusing on transmittance. I want to look closer to signaling.

-       Figuring out how to make this happen on a large scale. Getting people interested in knowing their microbiomes.
<br>
<br>
Even if it sounds kind of weird to scan yourself to gain information about your microbiomes I think it could be something like getting a kit from 23 and me where you submit a DNA sample to get information about your heritage. If you were able to sample your self and submit your results it could reveal secrets about ourselves and our health.
<br>
<br>
### Glitter mapping:

Covering myself in glitter was the second part of my experiment and it was the stronger part of my presentation. It was an easy way for me to embody my idea and it also sparked interest in the audience. It really was an experience to have it so visible on my skin and it clearly visualized how we impart bacteria on our environment because I could trace everywhere I touched with the glitter. The crowning moment was when Tomas called my name from the other end of the room and showed me that there was a streak of gold glitter on his jacket, which was quite funny but I also don’t know how it got there. It clearly shows how we are connected to the environment we live in and interact with.
<br>
<br>

- It is quite messy to have paint or glitter covering your body so how can frame it in a way to have people have the same experiences as me?

-       It is still an abstract idea and it sometimes felt like a stretch to connect the glitter to bacteria in my mind.

-       How can I make a stronger connection between the medium and the idea?

-       Can I use an existing habit/routine to attach the experience to?

-       Does it need to have the direct connection between the visual and the actual microbes?

-       Can it be a metaphor that communicates wellbeing without being directly linked to the bacteria?
<br>
<br>
#### Limitations:

-       currently I have only seen my personal effect on myself and the environment but I have not seen the effect other people have on me. Or the environment.

-       I impart matter on the environment but I also receive. I have not experimented with how that affects the experience.
<br>
<br>

###Matieral from the presentation
<br>
<br>
![](assets/Humans and bacteria slide.jpg)
![](assets/Biome scanner2.jpg)
![](assets/limitations.jpg)
<br>
<br>
![](assets/table setup.JPG)
![](assets/biome profiles2.jpg)
![](assets/presentation ready.JPG)
![](assets/presentation.JPG)
![](assets/aftermath.JPG)
*the glitter got everywhere but I didn't mind it so much and it only strengthened my storytelling*


### Feedback

Over all I got very positive feedback on my presentation and subject. The feedback was quite varied but I’m glad that I had the two different aspects to my presentation so everyone that I talked to could comment on how I could take each part further and different paths to take with the project. I am very enthusiastic to move forward with this project.

Explaining my project so many times to people with different expertise made me better able to explain my ideas and also made some aspects of the project clearer to myself. It was exhausting but rewarding.

One comment that was very helpful was that I should explore the steps needed to reach my theoretical goal and how different internal and external factors could affect that trajectory. Then also explore the mundane because when you look at the things we do and interact with in our daily lives can become very interesting when you look at them with a critical eye.

### Moving forward.

Diving into the science.

I have been focusing on the idea of the microbiome but not so much on the science behind it but I want to find a way to combine the science, keeping the information accurate but at the same time accessible to the public. It is always a challenge to create a space that appeals to both groups.

I also want to move forward with aspects from both parts of my presentation

An experience embedded with information.

### Reflections:

I feel a bit conflicted because I felt for the longest time that my area of intervention would be connected to climate change and the lecture we had with José Luis de Vicente still sits in me. I am still annoyed and angry when I think about that lecture but maybe it had a positive effect in the end because it made me more determined to not be like him and try to create awareness and educate in a more positive manner.

Climate change is one of the biggest and most complicated issues we have to deal with in our reality so maybe it’s not so strange that one person can’t solve it on their own. We need to change the fundamental way people think and act so maybe it’s good to start small. Even microscopic?

The exploration into bacteria was kind of accidental and when I started it was just a throwaway idea but it persisted and has become more interesting every time I revisit it. There are so many directions to take the exploration in but right now I want it to be an experience with a communicative aspect. Something people can feel for themselves and gain awareness while taking care of themselves.

If it is possible to get people to actively take care of their own personal ecosystems could that thinking be applied to taking care of the environment and make people care about the climate?
<br>
<br>
Wouldn’t it be amazing if exploration into bacteria could benefit the planet?

[Human Microbiome Project](https://www.hmpdacc.org/)<br>
[External Bacteria Information](https://www.thoughtco.com/bacteria-that-live-on-your-skin-373528)<br>
[Mother Dirt skin care](https://motherdirt.com/)<br>
[Tula skin care](https://www.tula.com/)<br>
