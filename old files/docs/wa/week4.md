#Exploring Hybrid profiles

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*Who am I ?   ---   Who do I want to become ?*<br>
<br>
This week we visited three designers and studios to get to know people that have successfully managed to combine their (sometimes unrelated) interests and incorporate them into their work. We went to a company called Domestic Data Streamers, Listened to a talk by Ariel Guersenzvaig and visited the studio of Sara d’Ubieta, a shoe designer that explores the possibilities of new materials in shoe making.<br>
<br>
I was asked by a fellow classmate what I meant by successful and I think that everyone we visited and listened to had gone an unconventional path to get where they are now. They seemed very content and were all doing very interesting things in exploration and research. It can be very challenging pursuing your passion and earning enough money to sustain yourself. If you manage to keep a good balance between those things, that is something I consider success.<br>
<br>
It was insightful to interact with these different designers and professionals and it served as an inspiration to us to start imagining what possible routes we could take towards our future careers.<br>
<br>
##VISITS

###Ariel Guersenzvaig
Ariel is an User interface and user experience designer but currently he refers to himself as an information architect. He has been involved in web developing for more than 20 years so he is very familiar with how digital interactions have evolved. He touched upon some concepts of interaction design like the affordances (communication of function), physical and digital.<br>
<br>
What I found most interesting was when he talked about the role of the designer and the responsibilities designers have. If designers are responsible for how the world should function, it should be really important that they study ethics. So they are aware of that their creations have consequences and can have a great influence on society. Designers are also not all knowing so they need to be aware of the people they are designing for. Not just find a problem and then design a solution and expect people to use it in the way they intended.<br>
<br>
I think that is one very important insight that I want to incorporate into my future work. To develop my designs in collaboration with the intended audience. It makes it more likely that your design will be successful and it also gives you better insights into the group you are designing for.<br>
<br>
###Sara d’Ubieta
We visited Sara’s studio, who is a shoe maker and designer in the outskirts of the city. She shared with us her transition from architecture into making and designing shoes and how she was frustrated with the limitations of traditional shoe making. She has been experimenting with new and non-traditional materials. She studied traditional shoe making for a long time so she knows how things were done and now she is free to experiment with different form and materials as she pleases. It was really interesting to hear her talk about the different materials she uses and you could clearly hear how passionate she is about her craft while she talked about exploring the properties of materials such as neoprene and aramite (a material used in military apparel). She has very interesting ideas about material how people frequently try to force new materials into the box of excising ones and how interesting it is to be inspired by the material and designing from there.<br>
<br>
![](assets/ShoeGraphic.jpg)<br>
<br>
The second half of the visit was more hands on and we were allowed to make our own shoe from scratch so we split into teams of 3 - 4 and were basically given free reign inside the studio and the only thing we had to use was insulation made from recycled cotton. I was in a team with [Barbara](https://mdef.gitlab.io/barbara.drozdek/) and [Veronica](https://mdef.gitlab.io/veronica.tran/) and we quickly agreed on a design that was quite simplistic and only needed torn up insulation thread and glue. We came up with a process with customized tools to soak the thread in glue and used that wrapped around the fabric to make the slipper. We worked in three different rooms so all the teams had very different results. I had so much fun, being able to make a shoe in about an hour. Just feeling the material in your hands and seeing what works and adjusting accordingly.<br>

![](assets/Shoe1.jpg)<br>
![](assets/Shoe-gif.gif)

###Domestic Data Streamers
The visit to DDS was very interesting, you would never think that a company of that caliber was residing across the street. I enjoyed all the visits but this was my favorite of the three. Data is becoming more important than ever and data collection is big business. But without someone to decipher the data it is only a vast collection of information. Their mantra is “making the invisible, visible” is true to me in two ways, it’s giving people a better and deeper understanding of the world and giving you an insight into how people behave and interact with the world.
One of the most interesting projects they talked about was the “Time Machine” project they did in collaboration with Unicef where they invited leaders and decision makers to an isolated and emotional journey telling them about a pervasive problem, and through that level of intimacy and data storytelling they were able to get those leaders to pledge their help and support.<br>
<br>
We humans are hardwired to react to stories so making data understandable and bringing it closer to people through stories is something that I want to do in the future, whether it is for companies that want a better relationship with their customers, governments to understand their societies or for people wanting to grasp their own patterns of living. The conversation around data and especially personal data has been a bit unsettling to me because we willingly give up our most intimate details to our gadgets and don’t really think or care about what the company or organization on the other side is using it for. That feels very uncomfortable but unavoidable at the same time so I want to help people more aware and connected with their data because when people start understanding they can start having a say in how their information is stored and used.<br>
The talk they gave reminded me of the first talk I went to when I started my Bachelors degree by a designer working for Frog design. That talk was about their research into how people in the United States freely share their prescription drugs to other patients when they are not using them anymore which made me speechless and determined to someday do research like that. Being able to visit DDS and seeing their cross disciplinary team of passionate, specialized people reconfirmed my desire of working with data and people.<br>
<br>
We spent the last hour doing a small workshop where we looked at the things we had us and in our bags and were tasked with organizing our things and trying to put an emotional layer on top of the things we had with us. It took a while but I organized five things that were in my bag in order of importance (mosquito cream, coffee mug, keys, wallet and phone) and then when I thought about it I realized that these items were also in order by how much anxiety each item gave me. The phone being the most important and gives me the most anxiety. It is amazing how much information is hidden in our every day things that we don’t realize but when we do it really makes us think.<br>
<br>
To sum up the three weeks I have three words:<br>
<br>
**Frustration – Curiosity – Discovery**<br>
<br>
These words are something that I think connect all three visits, we talked to people who were not satisfied with the way things were so they carved a new path by using their skills and interests which I think is amazing.<br>

##Reflection

In the reflection part of the week we each read one article from one of three categories (Vision / Reflect / Experience) I chose the Reflection article that was focusing on thinking critically about the information you gather and being able to communicate that information in a coherent way to others.<br>
<br>
Now I am a bit confused about myself because after the Domestic Data Streamers visit I feel like the thing I’m most passionate about is research and constructing engaging stories about information but then I chose the reflection article as the article to read. I don’t really see myself as a teacher but maybe conveying information in a relatable way is teaching? I have been having a really hard time trying to narrow down my vision and what I want to do. I feel like my interests pull me in different directions all the time and if I try to narrow it down I just get more confused. Some emerging interests are design decision making, ethics and governance.
