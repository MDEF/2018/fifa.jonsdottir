##01. MDEF BOOTCAMP

This week we started the MDEF program and we were introduced to key staff members, possible themes of research and the benefits of doing detailed documentation of your work, thoughts and experiences.


#Tuesday:
We all sat around in a circle and each member of the class explained in a few words their background, their expectations for the coming months and what they imagined doing in the future. It was really interesting to listen to everyone and it has made me even more excited to start developing ideas and working with everyone in the class. We noted down what subjects and key words we were particularly interested in and in the end of the class we divided ourselves into groups of 5-6 with similar interests. I had a hard time choosing between a biology/farming group and a circular systems group but I chose the latter because that is something that I want to learn more about and is something that I could contribute to with my knowledge of nature.

The key words that interested me the most were:

PRODUCTION<br>
CONSUMPTION<br>
SPACE<br>
INTERACTION<br>
ENGAGEMENT<br>
NATURE<br>


#Wednesday:
We had a class on documentation and got an introduction to Git Lab which is the platform we are going to use to work on our individual websites and together on collaborative projects. It took quite a while for the whole class to get set up but that is to be expected when you introduce a new program or platform to a group of people. Even though I don’t understand all the functions yet I can feel how important this tool is going to be for future collaborations. So it’s time to learn new terms and shortcuts!<br>


#Thursday:
Tomas introduced some of the projects that he has been working on. A lot of the work he has been doing is about extending the Fab Lab network and accelerating open source projects but what I thought was most interesting was a concept that I have heard many times before but constantly forget to think about which is hidden costs. What is the real cost of the products we consume? A T-shirt might be cheap to buy, but what consequences does the textile industry, transport and slave labor have on society and the planet?

After the lecture we went out with a guide to explore the Poublenou neighborhood to see some of the creative maker spaces that are in the streets close to the school. There are so many interesting projects going on so it will be interesting to get to know the people behind them and hopefully be able to collaborate with some of them and use them as resources in the near future. The strange thing to me is that this creative, close knit community seems pretty isolated from the other inhabitants of the city. Maybe it is because they have invested so much energy in starting and maintaining their spaces that they don’t have the resources to introduce what they do to a larger audience. I really want to know more about that.

For the last exercise on Thursday, me and my team (Gabriela, Katherine, Barbara, Tom and Nicolas) went out trash hunting and at 7 there were already piles of stuff on street corners. Me and Katherine found this gigantic frame in very good condition made from solid wood with purple material draped over it. It was very funny carrying it 4 blocks to school but it felt nice that because it was this night the streets are filled with people dropping off and picking up big furniture so it wasn’t seen as strange that we were carrying this massive purple item. It felt like we were a part of this adventure night that everyone is in on. The rest of the group found  some really nice intact shelves and some asphalt which will be interesting to see what we can do with.

![](assets/trashhunting.jpg)

#Friday:
Oscar started the day by telling us about his projects. The thing that stood out to me was that context is crucial when you design, not just for individuals but for different groups and different sections of society. There will never be one solution that fits everyone. Another thing is that you have to remember to challenge preconceptions you have of the world around you. Examine the words and actions you associate with each subject and try to break away from that. It takes a lot of practice and focus to be able to look at your environment with a different perspective.<br>


#Reflection:
During this week we have done a lot. I didn’t really know what to expect when I applied, but after this week it has become a little clearer, so here is a quick summary of my learning experiences this week.

•	Fast and in-depth introductions<br>
•	Exploring resources that are close by<br>
•	Adjusting to a new way of working<br>
•	Challenge what you think you know<br>
•	Exhaustion and excitement<br>
